#include <cassert>

//********************************************************
// This code sets symmetry ghostzones for hydro and MHD
// variables.
// It is a generalisation of Zach's original routines
//
//
// Written in September 2016 by Elias R. Most
//
//
//********************************************************

/*
constexpr static int HYDRO=0;
constexpr static int VECTOR_POTENTIAL=1;
constexpr static int MAGNETIC_FIELD=2;
*/

  constexpr static int sym_hydro[5][3]{
      {1, 1, 1},  // rho_star
      {1, 1, 1},  // tau
      {-1, 1, 1}, // mhd_st_x
      {1, -1, 1}, // mhd_st_y
      {1, 1, -1}  // mhd_st_z
  };
  constexpr static int sym_avec[4][3]{
      {1, 1, 1},  // psi6phi
      {-1, 1, 1}, // A_x
      {1, -1, 1}, // A_y
      {1, 1, -1}  // A_z
  };
  constexpr static int sym_bvec[6][3]{
      {1, -1, -1}, // B_x
      {-1, 1, -1}, // B_y
      {-1, -1, 1}, // B_z
      // staggered variables
      {1, -1, -1}, // B_x
      {-1, 1, -1}, // B_y
      {-1, -1, 1}  // B_z
  };

  constexpr static int stagger_hydro[5][3]{
      {0, 0, 0}, // rho_star
      {0, 0, 0}, // tau
      {0, 0, 0}, // mhd_st_x
      {0, 0, 0}, // mhd_st_y
      {0, 0, 0}  // mhd_st_z
  };
  constexpr static int stagger_avec[4][3]{
      {1, 1, 1}, // psi6phi
      {0, 1, 1}, // A_x
      {1, 0, 1}, // A_y
      {1, 1, 0}  // A_z
  };
  constexpr static int stagger_bvec[6][3]{
      {0, 0, 0}, // B_x
      {0, 0, 0}, // B_y
      {0, 0, 0}, // B_z
      // staggered variables
      {1, 0, 0}, // B_x
      {0, 1, 0}, // B_y
      {0, 0, 1}  // B_z
  };

class ReflectionSym {

public:
  enum { HYDRO = 0, VECTOR_POTENTIAL, MAGNETIC_FIELD };

  constexpr static int num_vars[3]{
      5, // HYDRO
      4, // VECTOR_POTENTIAL
      6  // MAGNETIC_FIELD
  };


  template <int which_vars>
  inline static void apply(const int *cctk_lsh, CCTK_REAL **vars,
                           const int which_sym[3], CCTK_REAL **coords) {
    switch (which_vars) {
    case HYDRO:
      if (which_sym[2])
        perform_copy<5, 2>(cctk_lsh, vars, coords, sym_hydro, stagger_hydro);
      if (which_sym[1])
        perform_copy<5, 1>(cctk_lsh, vars, coords, sym_hydro, stagger_hydro);
      if (which_sym[0])
        perform_copy<5, 0>(cctk_lsh, vars, coords, sym_hydro, stagger_hydro);
      break;
    case VECTOR_POTENTIAL:
      if (which_sym[2])
        perform_copy<4, 2>(cctk_lsh, vars, coords, sym_avec, stagger_avec);
      if (which_sym[1])
        perform_copy<4, 1>(cctk_lsh, vars, coords, sym_avec, stagger_avec);
      if (which_sym[0])
        perform_copy<4, 0>(cctk_lsh, vars, coords, sym_avec, stagger_avec);
      break;
    case MAGNETIC_FIELD:
      if (which_sym[2])
        perform_copy<6, 2>(cctk_lsh, vars, coords, sym_bvec, stagger_bvec);
      if (which_sym[1])
        perform_copy<6, 1>(cctk_lsh, vars, coords, sym_bvec, stagger_bvec);
      if (which_sym[0])
        perform_copy<6, 0>(cctk_lsh, vars, coords, sym_bvec, stagger_bvec);
      break;
    };
  };

  // Only z-reflection for now!
  constexpr static int which_sym_z[3]{0, 0, 1};

  // temporary convenience function as long as we only support z-symmetry
  template <int which_vars>
  inline static void apply(const int *cctk_lsh, CCTK_REAL **vars,
                           CCTK_REAL **coords) {
    apply<which_vars>(cctk_lsh, vars, which_sym_z, coords);
  };

private:
  constexpr static int indexGF(const int *cctk_lsh, int i, int j, int k) {
    return i + cctk_lsh[0] * (j + cctk_lsh[1] * k);
  };

  template <int sym_dir>
  constexpr static int indexSym(const int *cctk_lsh, int i, int j, int k,
                                int stagger) {
    return (sym_dir == 2)
               ? indexGF(cctk_lsh, i, j, stagger - k)
               : ((sym_dir == 1) ? indexGF(cctk_lsh, i, stagger - j, k)
                                 : indexGF(cctk_lsh, stagger - i, j, k));
  };
  template <int sym_dir>
  constexpr static int indexSym_2(const int *cctk_lsh, int i, int j, int k) {
    return (sym_dir == 2) ? indexGF(cctk_lsh, i, j, k)
                          : ((sym_dir == 1) ? indexGF(cctk_lsh, j, k, i)
                                            : indexGF(cctk_lsh, k, i, j));
  };

  // See the following comment by Zach for explanations:

  /* This loop sets symmetry ghostzones, regardless of how the gridfunction is
   * staggered.
   *
   * STAGGERED PATTERN:
   * if num_gzs==1 && stagger_z==1:
   * z[] = {-dz/2,dz/2,3dz/2, etc} -> gridfunc[index 0] =
   * gridfunc_syms[2]*gridfunc[index 1]
   *
   * if num_gzs==2 && stagger_z==1:
   * z[] = {-3dz/2,-dz/2,dz/2,3dz/2 etc}
   * -> gridfunc[index 0] = gridfunc_syms[2]*gridfunc[index 3]
   * -> gridfunc[index 1] = gridfunc_syms[2]*gridfunc[index 2]
   * .
   * .
   * .
   * -> gridfunc[i] = gridfunc_syms[2]*gridfunc[(num_gz*2-1)-i]
   *
   * UNSTAGGERED PATTERN:
   * if num_gzs==1 && stagger_z==0:
   * z[] = {-dz,0,dz, etc} -> gridfunc[index 0] =
   * gridfunc_syms[2]*gridfunc[index 2]
   *
   * if num_gzs==2 && stagger_z==0:
   * z[] = {-2dz,-dz,0,dz,2dz, etc} -> gridfunc[index 0] =
   * gridfunc_syms[2]*gridfunc[index 4] z[] = {-2dz,-dz,0,dz,2dz, etc} ->
   * gridfunc[index 1] = gridfunc_syms[2]*gridfunc[index 3]
   * .
   * .
   * .
   * -> gridfunc[i] = gridfunc_syms[2]*gridfunc[(num_gz*2)-i]
   *
   * OVERALL PATTERN: gridfunc[i] =
   * gridfunc_syms[2]*gridfunc[(num_gz*2-stagger_z)-i] */

  template <int N, int sym_dir>
  inline static void perform_copy(const int *cctk_lsh, CCTK_REAL **vars,
                                  CCTK_REAL **coords, const int parities[N][3],
                                  const int stagger[N][3]) {

    const CCTK_REAL delta =
        fabs(coords[sym_dir][indexSym_2<sym_dir>(cctk_lsh, 0, 0, 1)] -
             coords[sym_dir][indexSym_2<sym_dir>(cctk_lsh, 0, 0, 0)]) *
        0.5;

#pragma omp parallel for schedule(static)
    for (int which_var = 0; which_var < N; ++which_var) {
      const CCTK_REAL offset = delta * (stagger[which_var][sym_dir]);
      int bounds[3]{cctk_lsh[0], cctk_lsh[1], cctk_lsh[2]};

      // FIXME: We stick to Zach's way for the time being
      int ngzs = 0;
      while ((coords[sym_dir][indexSym_2<sym_dir>(cctk_lsh, 0, 0, ngzs)] +
              offset) < -delta / 5.0) {
        ++ngzs;
      };
      assert(2 * ngzs < cctk_lsh[sym_dir]);

      bounds[sym_dir] = ngzs;

      for (int kk = 0; kk < bounds[2]; ++kk)
        for (int jj = 0; jj < bounds[1]; ++jj)
          for (int ii = 0; ii < bounds[0]; ++ii) {
            const int in_gz = indexGF(cctk_lsh, ii, jj, kk);

            const int stride = (ngzs * 2 - stagger[which_var][sym_dir]);
            const int in_grid = indexSym<sym_dir>(cctk_lsh, ii, jj, kk, stride);

            vars[which_var][in_gz] =
                parities[which_var][sym_dir] * vars[which_var][in_grid];
          }
    }
  };
};
